# [pYTerm-QT](https://gitlab.com/mocchapi/pyterm-qt)
pyQT5 GUI for [pYTerm](https://gitlab.com/mocchapi/pyterminal), the simple youtube music player

![Main window](main_sc.png)
![Playlist window](list_sc.png)
## Installation

 1. Install [VLC Media Player,](https://www.videolan.org/vlc/) make sure to use the same architecture version as your python install (x86 or x64)
 2. Install the python dependencies with `python -m pip install -r requirements.txt`
###### (your path may default `python` to python 2.7, if this is the case use the alias of your python3 installation)

## Usage
To add songs to the queue, simply fill in search terms or a youtube link for the song you want to play in the search bar and click "search" or press enter.

If you want to play a playlist instead, write `playlist:` followed by either a link to a youtube playlist, the path to a local playlist file, or search terms to look for youtube playlists.

Volume levels are changed by the small circular knob on the bottom right of the player. Rotating it left will lower volume while rotating it right will increase it.

If you wish to skip ahead to any song you already queue'd up, click the queue button in the bottom left. This will open the queue window from which you can select any of the songs in the queue. You can also use the next and previous buttons.

At any point while a song is playing you can grab the timeline bar and drag it to any point to fast forward/backwards in a song.

Clicking the title of the currently playing song will open the youtube link in your default browser.
