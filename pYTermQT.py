#!/usr/bin/python3
import os
import sys
import json
import threading
import webbrowser
import urllib.request
from time import sleep

try:
    from pYTermLink import pYTerm
    print('using dev pYTerm')
except ImportError:
    from pYTerm import pYTerm

from PyQt5.uic import loadUi
from PyQt5.QtGui import QIcon, QMovie
from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication, QStyle, QLabel, QMessageBox
from PyQt5.QtCore import QByteArray, Qt, QSize, pyqtSignal, QObject
# from PyQt5.QtCore.Qt import 

root_dir = os.path.dirname(os.path.realpath(__file__))

def download(url):
    return urllib.request.urlretrieve(url, f'{root_dir}/tmp/{url.split("/")[-1]}')[0]

if os.name == 'posix':
    ICONS = {  'pause': QIcon.fromTheme('media-playback-pause'),
                'play': QIcon.fromTheme('media-playback-start'),
                'next': QIcon.fromTheme('media-skip-forward'),
                'prev': QIcon.fromTheme('media-skip-backward'),
                'shuffle': QIcon.fromTheme('media-playlist-shuffle'),
                'loop': QIcon.fromTheme('media-playlist-repeat'),
                'menu': QIcon.fromTheme('go-down'),
    }
else:
    ICONS = {	'pause':QIcon(QApplication.style().standardIcon(QStyle.SP_MediaPause)),
                'play':QIcon(QApplication.style().standardIcon(QStyle.SP_MediaPlay)),
                'next':QIcon(QApplication.style().standardIcon(QStyle.SP_MediaSkipForward)),
                'prev':QIcon(QApplication.style().standardIcon(QStyle.SP_MediaSkipBackward)),
                'shuffle':'🔀',
                'loop':QIcon(QApplication.style().standardIcon(QStyle.SP_BrowserReload)),
                'menu': QIcon(QApplication.style().standardIcon(QStyle.SP_TitleBarUnshadeButton)),
    }


# class SongInfo(QWidget):
#     def __init__(self, parent, *args, **kwargs:
#         super().__init__(parent)
#         pass


class Communicate(QObject):
    signal = pyqtSignal()

class playlistpopup(QWidget):
    def __init__(self, callback, *args, **kwargs):
        super().__init__(*args,**kwargs)
        loadUi('playlist.ui', self)
        self.window = None
        self.callback = callback
        self.buttons.accepted.connect(self.okay)
        self.buttons.rejected.connect(self.no)
    
    def no(self):
        if self.window != None:
            self.window.close()
        self.close()

    
    def okay(self):
        title = self.title.text()
        songs = self.songs.toPlainText().split('\n')
        if title and songs:
            self.callback(title, songs)
            self.no()

class Player(QWidget):
    def __init__(self, playlistdb=os.path.join(root_dir, 'playlists.json'),*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.playlistpath = playlistdb
        self.playlistdb = {}
        self.player = pYTerm.Player( rich_presence=True, enable_input=False, legacystreams=False, debuglogs=True, loop_queue=False )
        self.player.play_all( keep_alive=True )
        self.slider_unlocked = True
        self.loaders = 0
        self.prevalive = True
        self.initUI()
        self.initPlaylistdb()
        threading.Thread( target=self.uiUpdateRunner, daemon=True ).start()
    
    def start_loading(self):
        self.loaders += 1
        self.update_ld.signal.emit()
    
    def update_loader(self):
        self.loading.setVisible(self.loaders > 0)
        # self.loading_filler.setVisible(self.loaders == 0)

    def finish_loading(self):
        self.loaders -= 1
        self.update_ld.signal.emit()

    
    def initPlaylistdb(self):
        print(self.playlistpath)
        if os.path.isfile(self.playlistpath):
            with open(self.playlistpath, 'r') as f:
                self.playlistdb = json.loads(f.read())
            self.update_playlists_box()
        else:
            with open(self.playlistpath, 'w') as f:
                f.write(json.dumps({}))

    def uiUpdateRunner(self):
        while True:
            try:
                sleep(0.25)
                self.update_u.signal.emit()
            except AttributeError:
                pass
            except Exception as e:
                print(e)
    
    def updateUI(self):
        is_alive = self.player.is_alive()
        # print(is_alive)
        self.btn_pause.setEnabled(is_alive)
        self.time_slider.setEnabled(is_alive)
        self.btn_prev.setEnabled(self.player.song_index > 0)
        self.btn_next.setEnabled(len(self.player.songs) > self.player.song_index or (len(self.player.songs) > 0 and self.player.loop_queue))
        if is_alive:
            # print('boop')
            if self.slider_unlocked:
                self.time_slider.setValue(self.player.get_progress()*1000)
            self.song_title.setText(self.player.current_song.title)
            self.song_artist.setText(self.player.current_song.artist)
            self.time_curr.setText(self.player.get_current_time())
            self.time_total.setText(self.player.get_total_time())
            if self.player.is_paused():
                self.btn_pause.setIcon(ICONS["play"])
            else:
                self.btn_pause.setIcon(ICONS["pause"])
            if not self.prevalive:
                file = (download(self.player.current_song.get_thumbnail()))
                # styleSheet = """* {{ 
                # background-image: url("{0}");
                # background-repeat: no-repeat;
                # background-position: center;
                # background-attachment: 
                # fixed; background-size: fit;}}""".format(file)
                styleSheet = """* {{
                border-image: url({0}) 0 0 0 0 stretch stretch;
                border-width: 0px;
                }}""".format(file)
                # print(styleSheet)
                self.thumbnail.setStyleSheet(styleSheet)
                self.box_queue.setCurrentRow(self.player.song_index)
        elif self.prevalive:
            self.box_queue.setCurrentRow(len(self.player.songs))
            self.time_slider.setValue(0)
            self.thumbnail.setStyleSheet('*{ border-image: none }')
            # self.song_title.setText("WHAT")
            # self.song_artist.setText("how")
            self.song_title.setText("Nothing playing")
            self.song_artist.setText("search for a song")
            self.time_curr.setText("00:00")
            self.time_total.setText("00:00")
        self.prevalive = is_alive
    
    def initUI(self):
        loadUi('pYTermQT.ui', self)
        self.btn_list.click()
        self.btn_list.click()
        self.btn_clearqueue.clicked.connect(self.player.clear_songs)
        self.btn_edit_playlist.clicked.connect(lambda: self.open_edit_playlist_popup(self.box_playlists.currentItem()))
        self.btn_del_playlist.clicked.connect(lambda: self.confirm_del_playlist(self.box_playlists.currentItem()))
        self.btn_dupe_playlist.clicked.connect(lambda: self.duplicate_playlist(self.box_playlists.currentItem()))
        self.btn_loop.clicked.connect(lambda: self.player.set_loop_queue(self.btn_loop.isChecked()))
        self.btn_add_playlist.clicked.connect(self.open_new_playlist_popup)
        self.btn_save_to_playlist.clicked.connect(self.save_as_playlist)
        self.btn_clear_song.clicked.connect(self.remove_song)
        self.box_playlists.itemDoubleClicked.connect(self.open_edit_playlist_popup)
        self.box_queue.dropEvent = self.drop_playlist_to_queuebox
        self.btn_prev.setIcon(ICONS['prev'])
        self.btn_next.setIcon(ICONS['next'])
        self.btn_pause.setIcon(ICONS['pause'])
        self.btn_loop.setIcon(ICONS['loop'])
        self.btn_list.setIcon(ICONS['menu'])
        if type(ICONS['shuffle']) == str:
            self.btn_shuffle.setText(ICONS['shuffle'])
        else:
            self.btn_shuffle.setIcon(ICONS['shuffle'])
        self.btn_search.clicked.connect(self.click_search)
        self.btn_shuffle.clicked.connect(self.shuffle)
        self.btn_next.clicked.connect(self.player.next)
        self.btn_prev.clicked.connect(self.player.previous)
        self.btn_pause.clicked.connect(self.player.toggle_pause)
        self.time_slider.sliderPressed.connect(self.lock_slider)
        self.time_slider.sliderMoved.connect(self.lock_slider)
        self.time_slider.sliderReleased.connect(self.unlock_slider)
        self.dial_volume.valueChanged.connect(self.volumechange)
        self.box_queue.itemPressed.connect(self.playat)
        self.volumechange()

        movie = QMovie(f'{root_dir}/res/loading   .gif')
        self.loading.setScaledContents(True)
        self.loading.setAlignment(Qt.AlignCenter)
        self.loading.setMovie(movie)
        movie.setScaledSize(QSize(25,25))
        
        movie.start()
        self.loading.show()
        self.loading.hide()
        # self.loading_filler.show()
    
        self.update_ql = Communicate()
        self.update_ql.signal.connect(self.update_queue_list)
        self.update_ld = Communicate()
        self.update_ld.signal.connect(self.update_loader)
        self.update_u = Communicate()
        self.update_u.signal.connect(self.updateUI)
        

        self.update_queue_list()

    def remove_song(self):
        curr = self.box_queue.currentRow()
        if curr < len(self.player.get_songs()) and curr != -1:
            self.player.remove_song(curr)
            self.update_queue_list()
    

    def playat(self):
        self.player.play_at_index(self.box_queue.currentRow())

    def volumechange(self):
        self.dial_volume.setToolTip(f'Change the volume ({self.dial_volume.value()}%)')
        self.player.set_volume(self.dial_volume.value(), fadetime=0)

    def lock_slider(self):
        self.slider_unlocked = False

    def unlock_slider(self):
        self.player.set_progress(self.time_slider.value()/1000)
        self.slider_unlocked = True

    def click_search(self):
        # print('uh hi')
        search = self.search_box.text()
        def _run_(self, search):
            print('searching for song',search)

            if search.startswith('playlist:'):
                self.player.add_playlist(search[8:])
            else:
                self.player.add_song(search)
            # self.update_queue_list()
            self.finish_loading()
            self.update_ql.signal.emit()

        if search:
            self.start_loading()
            self.search_box.setText("")
            self.search_box.setFocus()
            threading.Thread(target=_run_, args=(self,search)).start()

    def shuffle(self):
        self.player.shuffle()
        self.update_queue_list()
    
    def update_playlists_box(self):
        self.box_playlists.clear()
        self.box_playlists.addItems(self.playlistdb.keys())


    def update_queue_list(self):
        self.box_queue.clear()
        new_list = []
        for song in self.player.songs:
            if type(song) == pYTerm.Song:
                new_list.append( song.artist +' - '+ song.title )
            else:
                new_list.append( song )
        new_list.append(' ')
        self.box_queue.addItems( new_list )
        self.box_queue.setCurrentRow(self.player.song_index)

    def saveplaylist(self):
        with open(self.playlistpath, 'w') as f:
            f.write(json.dumps(self.playlistdb))

    def add_new_playlist(self, title, songs):
        self.playlistdb[title] = songs
        self.update_playlists_box()
        self.saveplaylist()


    def save_as_playlist(self):
        songs = [a.vidtitle for a in self.player.get_songs()]
        if len(songs) > 0:
            newname = ''
            self.playlistdb[newname] = songs
            self.open_edit_playlist_popup(newname)
    
    def duplicate_playlist(self, playlist):
        if playlist == None: return
        if type(playlist) != str:
            playlist = playlist.text()
        if self.playlistdb.get(playlist):
            newname = playlist+' copy'
            self.playlistdb[newname] = self.playlistdb[playlist].copy()
            self.open_edit_playlist_popup(newname)
            # self.saveplaylist()

    def open_new_playlist_popup(self):
        dialog = playlistpopup(self.add_new_playlist)
        # dialog.show()
        window = QMainWindow(self)
        dialog.window = window
        window.setCentralWidget(dialog)
        window.show()
        del dialog
        del window
    
    def edit_playlist(self, oldtitle, newtitle, songs):
        if oldtitle != newtitle:
            del self.playlistdb[oldtitle]
        self.add_new_playlist(newtitle, songs)

    def open_edit_playlist_popup(self, key):
        if key == None: return
        if type(key) != str:
            key = key.text()

        if (x := self.playlistdb.get(key)):
            dialog = playlistpopup(lambda title, songs: self.edit_playlist(key, title, songs))
            dialog.title.setText(key)
            dialog.songs.setPlainText('\n'.join(x)) 
        # # dialog.show()
            window = QMainWindow(self)
            dialog.window = window
            window.setCentralWidget(dialog)
            window.show()
            del dialog
            del window
    
    def confirm_del_playlist(self, playlist):
        if playlist == None: return
        if type(playlist) != str:
            playlist = playlist.text()
        reply = QMessageBox.question(self, 'Confirmation', f'Are you sure you want to delete playlist "{playlist}"?',
        QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.remove_playlist(playlist)
        
        
    def remove_playlist(self, playlist):
        if playlist == None: return
        if type(playlist) != str:
            playlist = playlist.text()
        if self.playlistdb.get(playlist):
            del self.playlistdb[playlist]
            self.saveplaylist()
            self.update_playlists_box()

    def add_playlist_to_queuebox(self, playlist):
        if playlist == None: return
        if type(playlist) != str:
            playlist = playlist.text()

        def _run_(self, songs):
            self.start_loading()
            print('adding playlist songs to queuebox',songs)
            self.player.add_song(songs)
            self.update_ql.signal.emit()
            self.finish_loading()

        print(playlist)
        if (x := self.playlistdb.get(playlist)):
            print('lets go??')
            threading.Thread(target=_run_, args=(self, x)).start()


    def drop_playlist_to_queuebox(self, event):
        mime = event.mimeData()
        if 'application/x-qabstractitemmodeldatalist' in mime.formats():
            data = mime.data('application/x-qabstractitemmodeldatalist')
            text = bytes(data).decode('utf-8')
            text = text[25:] # theeres a buncha extra bytes and idk how to properly get the text so this is how we're doing it
            a = ''
            for idx,char in enumerate(text):
                if (idx % 2) == 1:
                    a += char # theres also random shit between the characters god help me
            text = a
            # print(data)
            # print(len(text))
            # print('"'+text+'"')
            # print(self.playlistdb)
            # print(self.playlistdb.get(text))
            self.add_playlist_to_queuebox(text)
        else:
            event.ignore()
        # print(event.mimeData())

def cleanup():
    for file in os.listdir(os.path.join(root_dir,'tmp')):
        os.remove(os.path.join(root_dir,'tmp',file))
        # print(os.path.join(root_dir,'tmp',file))

def main():
    app = QApplication(sys.argv)
    app.aboutToQuit.connect(cleanup)
    widget = Player()
    # widget.show()
    window = QMainWindow()
    window.setCentralWidget(widget)
    window.show()
    window.setWindowIcon(QIcon('pytermlogohalfbg.png'))
    app.exec()


if __name__ == '__main__':
    main()
